# What's it about?

This repository contains various scripts for building Python docker images for
various purposes.  Each individual subdirectory here has a README offering more
information on what it is about.

## Why not use the official Python images?

The Python official images have lot more stuff than required, not always
available for the specific version of Python you might need, and not always
available in the desired Linux base image.

Also, for the purposes of running the containers in production the use of
distroless python images are preferred, since they not only take less space but
also reduce the attack surface. While distroless Python images are available from
gcr.io, check this [README](./distroless/README.md) on why I have chosen
to build my own distroless images of Python.

## More information

Please navigate to the README in the relevant subdirectories for more
information.
