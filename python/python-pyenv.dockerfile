ARG BASE_BUILD_IMAGE
FROM $BASE_BUILD_IMAGE
LABEL maintainer="Ragav"

ARG PY_VER=3.9.2

ARG NONROOT_USER="pyrunner"
ARG NONROOT_GROUP="runner"

RUN apt-get update \
    && apt-get install -y \
    build-essential zlib1g-dev libncurses5-dev libgdbm-dev \
    libnss3-dev libssl-dev libsqlite3-dev libreadline-dev \
    libffi-dev curl libbz2-dev git

RUN groupadd ${NONROOT_GROUP} && \
    useradd -m ${NONROOT_USER} && adduser ${NONROOT_USER} ${NONROOT_GROUP}

USER ${NONROOT_USER}

ENV PATH="/home/${NONROOT_USER}/.pyenv/bin:${PATH}"

WORKDIR /home/${NONROOT_USER}
RUN curl https://pyenv.run | bash
RUN pyenv install -v $PY_VER
RUN pyenv global $PY_VER

USER root
RUN ln -s /home/${NONROOT_USER}/.pyenv/versions/${PY_VER}/bin/python /bin/python

USER ${NONROOT_USER}
WORKDIR /home/${NONROOT_USER}

# Install Poetry
RUN python -m pip install poetry
