#!/bin/bash
# author    Ragav N

script=$(basename $0)
script_dir=$(echo $0 | sed 's/\(^.*\)\/[^/]*$/\1/')

if [ $# -ne 3 ]; then
    echo "Usage: $script <dockerhub-username> <python-version>" \
         "<base_build_image>"
    exit 1
fi

username=$1
PY_VER=$2
BASE_BUILD_IMAGE=$3

docker_url='https://registry.hub.docker.com/v2/repositories'
repo_url="${docker_url}/${username}/pyenv-python/tags?page_size=1024"

my_versions=($(curl -L -s $repo_url  \
            | jq '."results"[]["name"]' \
            | tr -d '"' | grep "^[0-9]"))

echo "Found existing versions: ${my_versions[@]}"

if ! [[ " ${my_versions[@]} " =~ " ${PY_VER} " ]]; then
    my_versions+=(${PY_VER})
fi

for ver in ${my_versions[@]}; do
    echo "Building image for python version $ver now"
    tag="${username}/pyenv-python:${ver}"
    docker build \
        --pull \
        --build-arg PY_VER=${ver} \
        --build-arg BASE_BUILD_IMAGE=${BASE_BUILD_IMAGE} \
        -t $tag -f ${script_dir}/python-pyenv.dockerfile ${script_dir}
    docker push $tag
done
