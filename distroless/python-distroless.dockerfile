ARG PY_VER=3.9.2
FROM ragavan/pyenv-python:${PY_VER} as temp

FROM gcr.io/distroless/base

LABEL maintainer="Ragav"

# The following commands are used while building the image, they are removed at
# the end from the final image.
COPY --from=temp /bin/echo /bin/echo
COPY --from=temp /bin/rm /bin/rm
COPY --from=temp /bin/sh /bin/sh

# The following libraries are necessary for the commands above to work.
ENV LIB_DIR lib/x86_64-linux-gnu
COPY --from=temp ${LIB_DIR}/libc.so.6  ${LIB_DIR}
COPY --from=temp ${LIB_DIR}/libdl.so.2  ${LIB_DIR}
COPY --from=temp ${LIB_DIR}/libpthread.so.0  ${LIB_DIR}

COPY --from=temp ${LIB_DIR}/libz.so.1  ${LIB_DIR}
COPY --from=temp /lib64/ld-linux-x86-64.so.2  /lib64/ld-linux-x86-64.so.2 

RUN echo "runner:x:1000:pyrunner" >> /etc/group
RUN echo "pyrunner:x:1001:" >> /etc/group
RUN echo "pyrunner:x:1000:1001::/home/pyrunner:" >> /etc/passwd

# Remove the commands from the final image.
RUN rm /bin/sh /bin/echo /bin/rm

# Copy the symbolic link to the python executable
COPY --from=temp /bin/python /usr/bin/python

# Switch user to pyrunner
USER pyrunner

COPY --from=temp --chown=pyrunner:runner /home/pyrunner /home/pyrunner

ENTRYPOINT ["/bin/python"]
