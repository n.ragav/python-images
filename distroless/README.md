# Another python distroless image

The [distroless Python
images](https://console.cloud.google.com/gcr/images/distroless/GLOBAL)
maintained by Google do not get the latest versions of Python added even
for months after the official release. Hence the need for maintaining my own
Python distroless image.

## Prerequisites

- This script automatically pulls all the images from the repository belonging
  to the user, for example,
  [ragavan/python-distroless](https://hub.docker.com/repository/docker/ragavan/python-distroless),
  and for each tags in it creates a distroless version of that image.

- In the event the repository did not exist, the script would terminate with
  nothing to do.

- *Note*: You might want to configure your docker so it will have the
  permissions to push to your docker account. The details of that are out of
  scope for this document, but the procedure is described
  [here](https://docs.docker.com/engine/reference/commandline/login/).

## Building

The script can be run as follows:

```shell
./build-distroless.sh <dockerhub username>
```

Where,

- **dockerhub username**: The dockerhub username to which the image should be
  tagged with the version and pushed.
