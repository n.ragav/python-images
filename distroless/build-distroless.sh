#!/bin/bash
# author    Ragav N

set -eo pipefail

script=$(basename $0)
script_dir=$(echo $0 | sed 's/\(^.*\)\/[^/]*$/\1/')

if [ $# -ne 1 ]; then
    echo "Usage: $script <dockerhub-username>"
    exit 1
fi

username=$1

docker_url='https://registry.hub.docker.com/v2/repositories'
repo_url="${docker_url}/${username}/pyenv-python/tags?page_size=1024"

my_versions=($(curl -L -s $repo_url  \
            | jq '."results"[]["name"]' \
            | tr -d '"' | grep "^[0-9]"))

echo "Found existing versions: ${my_versions[@]}"

for ver in ${my_versions[@]}; do
    echo "Building distroless image for Python version ${ver} now"
    tag="${username}/python-distroless:${ver}"
    docker build \
        --pull \
        --build-arg PY_VER=${ver} \
        -t $tag -f ${script_dir}/python-distroless.dockerfile ${script_dir}
    docker push $tag
done
