# Author    Ragav N
#
# This file takes the distroless python image from ragavan/distroless at a
# specified version, and adds basic UNIX commands to it that helps examine its
# content at runtime. 
#
# This image, therefore, is to be strictly used for the purposes of debugging,
# and not in a production environment. There is no point of using a distroless
# image otherwise.

# Debian buster is Debian version 10
ARG version=3.7.3
FROM debian:buster as temp

FROM ragavan/python-distroless:${version}
LABEL maintainer="Ragav N"

COPY --from=temp /bin/chmod /bin/chmod
COPY --from=temp /bin/ls /bin/ls
COPY --from=temp /bin/rm /bin/rm
COPY --from=temp /bin/sh /bin/sh
COPY --from=temp /bin/which /bin/which

ENV LIB_DIR lib/x86_64-linux-gnu

# The following libraries are necessary for the commands above to work.
COPY --from=temp ${LIB_DIR}/libc.so.6  ${LIB_DIR}
COPY --from=temp ${LIB_DIR}/libdl.so.2  ${LIB_DIR}
COPY --from=temp ${LIB_DIR}/libpcre.so.3  ${LIB_DIR}
COPY --from=temp ${LIB_DIR}/libpthread.so.0  ${LIB_DIR}
COPY --from=temp ${LIB_DIR}/libselinux.so.1  ${LIB_DIR}

COPY --from=temp /lib64/ld-linux-x86-64.so.2  /lib64/ld-linux-x86-64.so.2 

ENTRYPOINT ["/bin/sh"]
