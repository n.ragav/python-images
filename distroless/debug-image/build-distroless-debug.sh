#!/bin/bash
# author    Ragav N
#
# The goal of this script is to create a distroless Python image exactly the
# same as the latest one in gcr.io/distroless/python<version>-debian10. If you
# are wondering why, please see the accompanying README available here:
# https://gitlab.com/n.ragav/python-images/-/blob/master/distroless/python/README.md

set -eo pipefail

script=$(basename $0)
script_dir=$(echo $0 | sed 's/\(^.*\)\/[^/]*$/\1/')

if [ $# -ne 1 ]; then
    echo "Usage: $script <dockerhub-username>"
    exit 1
fi

username=$1
docker_url='https://registry.hub.docker.com/v2/repositories'
repo_url="${docker_url}/${username}/python-distroless/tags?page_size=1024"
my_versions=($(curl -L -s $repo_url  \
            | jq '."results"[]["name"]' \
            | tr -d '"' | grep "^[0-9]"))

for v in ${my_versions[@]}; do
    tag="${username}/python-distroless:debug-$v"
    docker build --pull --build-arg version=$v -t $tag \
        -f ${script_dir}/python-distroless-debug.dockerfile ${script_dir}
    docker push $tag
done
